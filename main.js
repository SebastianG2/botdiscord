

//Node native filesystem module
const fs = require('fs');

//Get config data
const { prefix, token } = require('./config.json');

// Import the discord.js module
const Discord = require('discord.js');

// Create an instance of a Discord client
const client = new Discord.Client({ ws: { intents: ['GUILD_PRESENCES', 'GUILD_MEMBERS'] } });

//const client = new Discord.Client({ ws: { intents: 402910294 } });

//importar modulo de comandos
client.commands = new Discord.Collection();
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
console.log(commandFiles);
for (const file of commandFiles) {
    console.log(file);
	const command = require(`./commands/${file}`);
	client.commands.set(command.name, command);
}

/**
 * The ready event is vital, it means that only _after_ this will your bot start reacting to information
 * received from Discord
 */
client.once('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});



 // ======Event listener para nuevos miembros======
 client.on("guildMemberAdd", (member) => {
    console.log(`New User "${member.user.username}" has joined "${member.guild.name}"` );
    member.guild.channels.cache.find(c => c.name === "general").send(`"${member.user.username}" has joined this server`);
  });



//======Comandos======

client.on('message', message => {
    console.log("entro al mensaje");
	if (!message.content.startsWith(prefix) || message.author.bot) {
        console.log("entro al return");
        return;
    }
    console.log("entro al prefijo");
	const args = message.content.slice(prefix.length).trim().split(/ +/);
	const command = args.shift().toLowerCase();

    if (!client.commands.has(command)) return;

    try {
        console.log("entro al try");
	    client.commands.get(command).execute(message, args);
    } catch (error) {
        console.error(error);
        message.reply('there was an error trying to execute that command!');
    }
	// other commands...
});









// client.emit("guildMemberAdd", "Academic");


//=====Cachar errores=====
client.on("error", (e) => console.error(e));
client.on("warn", (e) => console.warn(e));
client.on("debug", (e) => console.info(e));



client.login(token);
