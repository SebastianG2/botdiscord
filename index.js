
//node filesystem module
const fs = require('fs')
// Importing this allows you to access the environment variables of the running node process
require('dotenv').config();
//discord module
const Discord = require('discord.js');
//const { prefix, token } = require('./config.json');
const prefix = process.env.PREFIX;
const client = new Discord.Client();
client.commands = new Discord.Collection();
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));
const cooldowns = new Discord.Collection();

//get the files on commandFiles
for (const file of commandFiles) {
	const command = require(`./commands/${file}`);

	// set a new item in the Collection
	// with the key as the command name and the value as the exported module
	client.commands.set(command.name, command);
}
//Bot ready to work?
client.once('ready', () => {
	console.log('Ready!');
});



//if user sends message with commands
client.on('message', message => {
    //check if message has prefix or was not sended by a bot
    if (!message.content.startsWith(prefix) || message.author.bot) return;

    const args = message.content.slice(prefix.length).trim().split(/ +/);
    const commandName = args.shift().toLowerCase();
    const command = client.commands.get(commandName) || client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));
	console.log(message.content);
    
    if (!command) return;

    if (command.guildOnly && message.channel.type === 'dm') {
        return message.reply('No puedo ejecutar eso en un mensaje directo');
    }

    if(command.permissions) {
        const authorPerms = message.channel.permissionsFor(message.author);
        if (!authorPerms || !authorPerms.has(command.permissions)) {
            return message.reply('No tienes permisos para ejecutar este comado');
        }
    }

    //check if command has args
    if (command.args && !args.length) {
        let reply = `No mandaste ningún argumento carnalito, ${message.author}!`;
        //Call usage help (in command file if exists)
		if (command.usage) {
			reply += `\nEl uso de este comando sería: \`${prefix}${command.name} ${command.usage}\``;
		}

		return message.channel.send(reply);
    }

    if (!cooldowns.has(command.name)) {
        cooldowns.set(command.name, new Discord.Collection());
    }
    
    const now = Date.now();
    const timestamps = cooldowns.get(command.name);
    const cooldownAmount = (command.cooldown || 3) * 1000;
    
     if (timestamps.has(message.author.id)) {
        const expirationTime = timestamps.get(message.author.id) + cooldownAmount;

        if (now < expirationTime) {
            const timeLeft = (expirationTime - now) / 1000;
            return message.reply(`please wait ${timeLeft.toFixed(1)} more second(s) before reusing the \`${command.name}\` command.`);
        }
    }
    timestamps.set(message.author.id, now);
    setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);


    try {
        command.execute(message, args);
    } catch (error) {
        console.error(error);
        message.reply('Ocurió un error al ejecutar ese comando');
    } 
    
});//ends message event

//Server greeting event
client.on("guildMemberAdd", (member) => {
    const mensajeBienvenida = new Discord.MessageEmbed()
	.setColor('#ab00ff')
	.setTitle('Bienvenido/a!')
	.setURL('https://www.rematchgd.com/')
	.setAuthor('RematchGD', 'https://rematchgd.com/img/logo2.png', 'https://rematchgd.com/img/logo2.png')
	.setDescription(`Bienvenido ${member.user.username} al servidor de ${member.guild.name}`)
	.setThumbnail('https://rematchgd.com/img/logo2.png')
	.addFields(
		{ name: 'Dato importante!', value: 'Puedes usar el comando `!help` para listar todos los comandos disponibles.' },
        { name: 'Recuerda pedir tu rol', value: 'El rol te dará acceso a los canales de los diferentes torneos. Usa el comando `!rol` para asignarte el rol deseado' },
		{ name: '\u200B', value: '\u200B' },
        { name: 'Nuestro equipo', value: ':)' },
		{ name: 'Academic', value: 'Admin', inline: true },
		{ name: 'Chemo', value: 'Admin', inline: true },
        { name: 'Derftech', value: 'Admin', inline: true }
	)
	.addField('Recuerda consultar nuestro aviso de privacidad', 'https://blog.rematchgd.com/avisodeprivacidad/', true)
	.setImage('')
	.setTimestamp()
	.setFooter('RematchGD', 'https://rematchgd.com/img/logo2.png');

    console.log(`New User "${member.user.username}" has joined "${member.guild.name}"` );
    //member.guild.channels.cache.find(c => c.name === "general").send(`Bienvenido "${member.user.username}" al servidor de "${member.guild.name}"`);
    member.guild.channels.cache.find(c => c.name === "general").send(mensajeBienvenida);
});//ends greeting event
  

console.log(process.env);

client.login();