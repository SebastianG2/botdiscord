# Bot de Discord
## Introducción
Este chat bot fue generado para apoyar a la administración de torneos de videojuegos y mantener a los usuarios del chat con interés.

## Pre-requisitos
Principalmente e utilizó node.js, la biblioteca discord.js y el conector para mysql.
Todos los paquetes utilizados los puedes consultar en package-lock.json

## Comandos
Los comandos desarrollados se encuentran en la carpeta "commands" donde se tiene un archivo js por cada comando.
Comandos implementados:
- help: Comando que lista ayudas al usuario.
- prune: Comando para eliminar N cantidad de mensajes donde solo los usuarios con cierto rol pueden ejecutarlo.
- reload: Comando para refrescar los comandos disponibles.
- rol: Comando que permite autoasignarse un rol.
- reporte-rocket: Comando que permite subir el reporte de una partida (los datos se envían a una base de datos MySQL).
- torneos: Comando que lista los torneos disponibles (los datos retornados se traen de una base de datos MySQL).  

## Archivo principal
El archivo principal de ejecución es index.js
