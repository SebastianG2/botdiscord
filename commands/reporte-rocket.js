const fs = require('fs');
const { createConnection } = require('mysql');
require('dotenv').config();
module.exports = {
	name: 'reporte-rocket',
	description: 'Guarda los resultados de una partida finalizada de RL junto con la imagen de evidencia.',
    args: true,
    usage: '<equipo1> <equipo2> <score-equipo1> <score-equipo2> <equipo-ganador> <imagen-evidencia>',
	execute(message, args) {
        const team1 = args[0];
        const team2 = args[1];
        const goals1 = args[2];
        const goals2 = args[3];
        const ganador = args[4];        
        var Attachment = (message.attachments).array();
        var evidencia = Attachment[0].url
        console.log(team1 + team2 + goals1 + goals2 + ganador);
       
        const database = createConnection({
            host: process.env.HOST,
            user: process.env.USER,
            password: process.env.PASSWORD,
            database: process.env.DATABASE,
          });
          

        try {
            database.connect();
            console.log("logueadito");
        } catch (error) {
            console.error(error);
            return;
            
        } 
        
        if(team1.toString() == team2.toString()){
            message.reply('Los equipos no pueden ser iguales')
            return;
        }
          

        database.query(`INSERT INTO reporte_partidas_rocket (team_a, team_b, score_a, score_b, ganador, evidencia) values('${team1}','${team2}','${goals1}','${goals2}','${ganador}','${evidencia}')  `, function (err, result2) {
            if(err) {
                message.reply('Ocurió un error al registrar tu partida, contacta con un administrador');
                console.log(err);
                return console.log('Error1');
            }else{
                message.reply('Tu partida se registró exitosamente');
                console.log("guardao");
                database.end(function(err) {
                    if (err) {
                      return console.log('error:' + err.message);
                    }
                    console.log('Closed the database connection.');
                  });
            }
        });

        
	},
};