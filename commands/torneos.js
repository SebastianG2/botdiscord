const fs = require('fs');
const { createConnection } = require('mysql');
const Discord = require('discord.js');
require('dotenv').config();
module.exports = {
	name: 'torneos',
	description: 'Muestra los torneos activos.',
    args: false,
	execute(message, args) {
        let sql;
        let embed = new Discord.MessageEmbed();
        var indice = 0;
        //var url_registro;       
        const database = createConnection({
            host: process.env.HOST,
            user: process.env.TUSER,
            password: process.env.TPASSWORD,
            database: process.env.TDATABASE,
          });
          

        try {
            database.connect();
            console.log("logueadito");
            sql = `CALL spSeleccionTorneos()`;
            database.query(sql, (error, results, fields) => {
                if (error) {
                    return console.error(error.message);
                }
                
                console.log(results);
                console.log('================================');
                console.log(results[0][0])
                
                //results[0].forEach(element => console.log(element));
                embed.setTitle('Torneos activos:');
                embed.setColor('#1cb0d4');
                embed.setAuthor('RematchGD', 'https://rematchgd.com/img/logo2.png', 'https://rematchgd.com/img/logo2.png')

                results[0].forEach(function(i) {
                    indice++;
                    console.log(i.nombre);
                    //embed.setDescription('Torneo: '+i.nombre);
                    //url_registro = i.url_registro;
                    embed.addField(indice+'. Torneo: '+i.nombre,'Registro y más información en: '+i.url_registro);
                    
                });
                message.channel.send(embed);
                
              });
            

        } catch (error) {
            console.error(error);
            return;
            
        }finally{
            database.end();
        } 
        
       
          

        

        
	},
};