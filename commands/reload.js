module.exports = {
	name: 'reload',
    description: 'Recarga los comandos',
    args: true,
    usage: '<command-to-reload>',
	execute(message, args) {
		const commandName = args[0].toLowerCase();
        const command = message.client.commands.get(commandName) || message.client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

    if (!command) return message.channel.send(`No existe un comando con ese nombre o alias \`${commandName}\`, ${message.author}!`);
    }
};
