module.exports = {
	name: 'kick',
	description: 'Kick user',
    permissions: 'KICK_MEMBERS',
    guildOnly: true,
	execute(message, args) {
		if (!message.mentions.users.size) {
            return message.reply('debes taggear un usuario para poder sacarlo del server!');
        }
        const taggedUser = message.mentions.users.first();
    
        message.channel.send(`Intentaste sacar del server a: ${taggedUser.username}`);
	},
};