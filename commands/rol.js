module.exports = {
	name: 'rol',
	description: 'Asigna un rol',
    args: true,
    usage: '<rol> (ejemplo de rol: rocket)',
	execute(message, args) {
		console.log(message.member.roles.highest);
		console.log(message.member.roles.cache)
		const role_i = args[0];
		
		if(role_i==="rocket"){
			//var role= message.member.guild.roles.cache.find(role => role.name === "Rocket League");
			//message.guild.members.roles.add(role);
			let role = message.member.guild.roles.cache.find(role => role.name === "RocketLeague");
			if (role) message.guild.members.cache.get(message.author.id).roles.add(role);
			message.channel.send("Se te asigno el Rol de RocketLeague!");
		}else{
			message.channel.send("Ese no parece ser un rol, los roles disponibles son: rocket");
		}
		
				
	},
};