const { prefix } = require('../config.json');
module.exports = {
	name: 'help',
	description: 'Muestra todos los comandos o información sobre un comando específico.',
	aliases: ['commands'],
	usage: '[nombre de comando]',
	cooldown: 5,
	execute(message, args) {
		const data = [];
        const { commands } = message.client;

        if (!args.length) {
            data.push('Aquí hay una lista con todos mis comandos disponibles:');
            data.push(commands.map(command => command.name).join(', '));
            data.push(`\nPuedes usar \`${prefix}help [comando]\` para obtener información de un comando en específico`);

            return message.author.send(data, { split: true })
                .then(() => {
                    if (message.channel.type === 'dm') return;
                    message.reply('Te envié mensaje directo con todos los comandos disponibles :)');
                })
                .catch(error => {
                    console.error(`No pude mandarte mensaje directo ${message.author.tag}.\n`, error);
                    message.reply('Parece que no puedo mandarte mensaje directo, tienes los DM´s desactivados?');
                });
        }
        const name = args[0].toLowerCase();
        const command = commands.get(name) || commands.find(c => c.aliases && c.aliases.includes(name));

        if (!command) {
            return message.reply('ese no es un comando válido!!');
        }

        data.push(`**Nombre:** ${command.name}`);

        if (command.aliases) data.push(`**Alias:** ${command.aliases.join(', ')}`);
        if (command.description) data.push(`**Descripción:** ${command.description}`);
        if (command.usage) data.push(`**Modo de uso:** ${prefix}${command.name} ${command.usage}`);

        data.push(`**Cooldown:** ${command.cooldown || 3} segundos(s)`);

        message.channel.send(data, { split: true });
	},
};