module.exports = {
	name: 'user-info',
	description: 'Muestra los datos de usuario',
	execute(message, args) {
		message.channel.send(`Tu nombre de usuario: ${message.author.username}\nTu ID: ${message.author.id}`);
	},
};