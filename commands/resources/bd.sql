-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
-- -----------------------------------------------------
-- Schema rematchbot
-- -----------------------------------------------------
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`partidas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`partidas` ;

CREATE TABLE IF NOT EXISTS `mydb`.`partidas` (
  `idpartidas` INT NOT NULL AUTO_INCREMENT,
  `team_a` VARCHAR(45) NOT NULL,
  `team_b` VARCHAR(45) NOT NULL,
  `score_a` INT NOT NULL,
  `score_b` INT NOT NULL,
  `ganador` VARCHAR(45) NOT NULL,
  `evidencia` VARCHAR(230) NOT NULL,
  UNIQUE INDEX `idpartidas_UNIQUE` (`idpartidas` ASC))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
