module.exports = {
	name: 'prune',
	description: 'Borra mensajes (solo admins)',
	execute(message, args) {
		const amount = parseInt(args[0]) + 1;
    
        if (isNaN(amount)) {
            return message.reply('eso no parece un número válido.');
        }
        else if (amount <= 1 || amount > 100) {
            return message.reply('debes ingresar un número entre 1 y 99.');
        }
        if(message.member.roles.cache.has('809605885731143720')){
            console.log('Es admin')
            message.channel.bulkDelete(amount, true).catch(err => {
                console.error(err);
                message.channel.send('ocurrió un error al procesar ese comando!');
            });
        }else{
            console.log("es gei")
            message.channel.send('Solo los todopoderosos admin pueden ejecutar este comando :C');
            return;
        }
        
	},
};